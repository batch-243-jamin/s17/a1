/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function myInfo(){
	alert("Hi! Please fill this up!");
	let fullName = prompt("Enter your full name:"); 
	let myAge = prompt("Enter your age:"); 
	let myLocation = prompt("Enter your location:");

	console.log("Hello, " + fullName + ".");
	console.log("You are " + myAge + " years old."); 
	console.log("You live in " + myLocation + "."); 
 
};

myInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function myFavoriteBands(){
		console.log("1. My Chemical Romance");
		console.log("2. Paramore");
		console.log("3. The 1975");
		console.log("4. The All-American Rejects");
		console.log("5. Falling in Reverse");
	}

	myFavoriteBands();





/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function myFavoriteMovies(){
		console.log("1. Black Panther");
		console.log("Rotten tomatoes Rating: 96%")
		console.log("2. Smile");
		console.log("Rotten tomatoes Rating: 79%")
		console.log("3. Barbarian");
		console.log("Rotten tomatoes Rating: 92%")
		console.log("4. All Quiet on the Western Front");
		console.log("Rotten tomatoes Rating: 92%")
		console.log("5. Enola Holmes 2");
		console.log("Rotten tomatoes Rating: 93%")
	}

	myFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);
